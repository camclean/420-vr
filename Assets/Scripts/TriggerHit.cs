﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerHit : MonoBehaviour
{
    public PointsManager pointsManager;
    public TextMesh floatingText;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            pointsManager.plusPoints();
            Destroy(other.gameObject);

            TextMesh instantiatedFloatingText = Instantiate(floatingText, transform.position, transform.rotation);
            instantiatedFloatingText.transform.eulerAngles = new Vector3(instantiatedFloatingText.transform.eulerAngles.x ,
                                                                       instantiatedFloatingText.transform.eulerAngles.y-90,
                                                                       instantiatedFloatingText.transform.eulerAngles.z);
            instantiatedFloatingText.gameObject.SetActive(true);
            Destroy(instantiatedFloatingText.gameObject, 0.5f);

            Destroy(this.gameObject, 3);
        }
    }
}