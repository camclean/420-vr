﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PointsManager : MonoBehaviour
{
    public int points = 0;
    public Text text;

    private void FixedUpdate()
    {
        text.text = "Points: " + points;
    }

    public void plusPoints()
    {
        points++;
        Debug.Log(points);
    }
}
