﻿using UnityEngine;
using System.Collections;
using Valve.VR;
using UnityEngine.Animations;


public class Shoot : MonoBehaviour
{  
    public Rigidbody projectile;
    public Animator anim;
    public float speed = 0.1f;
    public SteamVR_Action_Boolean m_FireAction = null;
    private SteamVR_Behaviour_Pose m_Pose = null;
    public SteamVR_Skeleton_Pose hand;

    public void Shootgun()
    {
        if (m_FireAction.GetStateDown(m_Pose.inputSource))
        {
            Rigidbody instantiatedProjectile = Instantiate(projectile, transform.position, transform.rotation);

            instantiatedProjectile.transform.eulerAngles = new Vector3(instantiatedProjectile.transform.eulerAngles.x + 90,
                                                                       instantiatedProjectile.transform.eulerAngles.y,
                                                                       instantiatedProjectile.transform.eulerAngles.z);

            instantiatedProjectile.velocity = transform.TransformDirection(new Vector3(0, 0, speed));

            Destroy(instantiatedProjectile.gameObject, 3);
        }
    }
}
